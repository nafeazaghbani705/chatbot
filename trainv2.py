import csv
import re
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from keras.models import Sequential
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences
from keras.models import  Sequential
from keras.layers import Dense ,Flatten,LSTM,Dropout,Activation,Embedding,Bidirectional,SimpleRNN, SpatialDropout1D
import nltk
import seaborn as sns
from keras import layers
from torch import embedding
import pandas as pd
from sklearn.model_selection import train_test_split
nltk.download('stopwords')
from nltk.corpus import stopwords

from keras.callbacks import EarlyStopping
   
REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
STOPWORDS =set(stopwords.words('french'))

def clean_text(text):
   
    text = text.lower() 
    text = REPLACE_BY_SPACE_RE.sub(' ', text) 
    text = BAD_SYMBOLS_RE.sub('', text)  
    text = ' '.join(word for word in text.split() if word not in STOPWORDS) 
    return text

df = pd.read_csv('data.csv')
print(df.head())
print(df.columns)
data = df[['Description', 'Catégorie']] 
data = data.dropna()

data.Catégorie.loc[(data['Catégorie'] == "Demande Matériel > Consommable Ordinateur") | (data['Catégorie'] == "Demande Matériel > Consommable imprimante") | 
         (data['Catégorie'] == "Demande Matériel > Pièce de rechange ordinateur") | (data['Catégorie'] == "Demande Matériel > Consommable imprimante > l310") | 
         (data['Catégorie'] == "Demande Matériel > M300 ") | (data['Catégorie'] == "Demande Matériel > Matériel Réseau (câbles,switch)") | 
         (data['Catégorie'] == "Demande Matériel > Consommable imprimante > M300") | (data['Catégorie'] == "Demande Matériel > Consommable imprimante > M200") |
         (data['Catégorie'] == "Demande Matériel > Consommable imprimante > M2300") | (data['Catégorie'] == "Demande Matériel > M200") |
         (data['Catégorie'] == "Demande Matériel > Consommable imprimante > 2571-N") | (data['Catégorie'] == "Demande Matériel > M2300") |
         (data['Catégorie'] == "Demande Matériel > Consommable imprimante > ML-2010") | (data['Catégorie'] == "Demande Matériel > Consommable imprimante > Epson M1400") |
         (data['Catégorie'] == "Demande Matériel > 2571-N") | (data['Catégorie'] == "Demande Matériel > lexmark E260") | 
         (data['Catégorie'] == "Demande Matériel > M1400") | (data['Catégorie'] == "Demande Matériel > Consommable imprimante > XEROX 6300") |
         (data['Catégorie'] == "Demande Matériel > Consommable imprimante > HP 600") | (data['Catégorie'] == "Demande Matériel > Consommable imprimante > photoconducteur") |
         (data['Catégorie'] == "Demande Matériel > photoconducteur") | (data['Catégorie'] == "Demande Matériel > M300") 
          | (data['Catégorie'] == "Demande Matériel > XEROX 6300") | (data['Catégorie'] == "Demande Matériel > HP 600")] = "Demande Matériel"

   
data.Catégorie.loc[(data['Catégorie'] == "Réparation imprimante")]=".Réparation Matériel Défectueux"




data.Catégorie.loc[(data['Catégorie'] == "Assistance > Installation logiciel") | (data['Catégorie'] == "Assistance > Installation imprimante") | 
                   (data['Catégorie'] == "Assistance > Configuration Impression Ivara") ]="Assistance"




data.Catégorie.loc[(data['Catégorie'] == "Réseau > Autorisation Internet") ]="Réseau"




data.Catégorie.loc[(data['Catégorie'] == "Administration > Ivara") | (data['Catégorie'] == "Administration > Ivara > Modification Profil Ivara") | 
                   (data['Catégorie'] == "Administration > Ivara > Création Compte Ivara") ]="Administration"

data.Catégorie.loc[(data['Catégorie'] == "Intranet > Création Compte Intranet") | (data['Catégorie'] == "Intranet > Reset Mot de passe Intranet") | 
                   (data['Catégorie'] == "Intranet > Modification droits compte Ged") ]="Intranet"   


data.Catégorie.loc[(data['Catégorie'] == "Sécurité > Kaspersky")]="Sécurité" 


data.Catégorie.loc[(data['Catégorie'] == "Session Windows > Reset Mot de passe session windows") | (data['Catégorie'] == "Session Windows > Création utilisateur selon matricule")]="Session Windows"   


data.Catégorie.loc[(data['Catégorie'] == "SI > Système inactif") | (data['Catégorie'] == "SI > Modification compte SI") | 
                   (data['Catégorie'] == "SI > Création Compte Si")  | (data['Catégorie'] == "SI > Problème sécurité (Java ..)")]="SI"

data.Catégorie.loc[(data['Catégorie'] == "En attente Arrivage ordinateur") | (data['Catégorie'] == "En attente Arrivage matériel") | 
                   (data['Catégorie'] == "en attente arrivage onduleur")  | (data['Catégorie'] == "En attente Arrivage consommable imprimante") |
                   (data['Catégorie'] == "En attente Arrivage imprimante ou scanner") | (data['Catégorie'] == "En attente Arrivage consommable imprimante")
                   | (data['Catégorie'] == "Réception Consommable")]="Autre"




print(data.head(1000))
print(data.shape)
print(data.dtypes)
print("h")
print(data.Catégorie.value_counts())
data['Description'] = data['Description'].apply(clean_text)
data['Catégorie'] = data['Catégorie']


MAX_NB_WORDS = 70000
MAX_SEQUENCE_LENGTH = 500
EMBEDDING_DIM = 300

tokenizer = Tokenizer(num_words=MAX_NB_WORDS, filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~', lower=True)
tokenizer.fit_on_texts(data['Description'].values)
word_index = tokenizer.word_index
print('Found %s unique tokens.' % len(word_index))
X = tokenizer.texts_to_sequences(data['Description'].values)
X = pad_sequences(X, maxlen=MAX_SEQUENCE_LENGTH)
print('Shape of data tensor:', X.shape)
Y = pd.get_dummies(data['Catégorie']).values
print('Shape of label tensor:', Y.shape)
X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size = 0.10, random_state = 42)
print(X_train.shape,Y_train.shape)
print(X_test.shape,Y_test.shape)

model = Sequential()
model.add(Embedding(MAX_NB_WORDS, EMBEDDING_DIM, input_length=X.shape[1]))
model.add(SpatialDropout1D(0.2))
model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
model.add(Dense(13, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

print(model.summary())
epochs = 5
batch_size = 64
epochs = 10
batch_size = 64


history = model.fit(X_train, Y_train, epochs=epochs, batch_size=batch_size,validation_split=0.1,callbacks=[EarlyStopping(monitor='val_loss', patience=3, min_delta=0.0001)])


accr = model.evaluate(X_test,Y_test)
print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accr[0],accr[1]))

plt.title('Loss')
plt.plot(history.history['loss'], label='train')
plt.plot(history.history['val_loss'], label='test')
plt.legend()
plt.show()
plt.title('Accuracy')
plt.plot(history.history['accuracy'], label='train')
plt.plot(history.history['val_accuracy'], label='test')
plt.legend()
plt.show()


message = ["Bonjour,Prière créer un compte intranet pour l'employé Chiraz Ghodhban Mle 91591.Merci. "]
seq = tokenizer.texts_to_sequences(message)
padded = pad_sequences(seq, maxlen=MAX_SEQUENCE_LENGTH)
pred = model.predict(padded)
labels = ['Demande Matériel','.Réparation Matériel Défectueux','Assistance','Administration','Autre','Réseau','Intranet','Session Windows','SI','Sécurité','Préparation Matériel (image ..)','livraison matériel','Réception Matériel']
print(pred, labels[np.argmax(pred)])