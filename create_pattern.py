import json
import csv


with open("mt.csv","r") as f:
    reader=csv.reader(f)
    next(reader)
    data={"":[]}
    for row in reader:
        data[""].append({"label":row[0],"pattern":row[1]})
        with open("mt.json","w") as f :
            json.dump(data,f,indent=4)
    