import csv
import re
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from keras.models import Sequential
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences
from keras.models import  Sequential
from keras.layers import Dense ,Flatten,LSTM,Dropout,Activation,Embedding,Bidirectional,SimpleRNN
import nltk
from keras import layers
from torch import embedding
def clean_categorie(categorie):
    data_pattern ="\>.*$"
    return(re.sub(data_pattern,"",categorie))
def clean_categorie2(categorie):
    data_pattern ="\(.*$"
    return(re.sub(data_pattern,"",categorie))


nltk.download('stopwords')
from nltk.corpus import stopwords
STOPWORDS =set(stopwords.words('french'))
vocab_size=5000
embedding_dim=64
max_length=200
oov_tok='<oov>'
training_portion=.08
messages=[]
cat=[]
with open("data.csv","r") as csvfile:
    reader=csv.reader(csvfile ,delimiter=",")
    next(reader)
    for row in reader:
        cat.append(clean_categorie2(clean_categorie(row[0])))
        message=row[1]
        for word in STOPWORDS:
            token=' ' + word + ' '
            message= message.replace( token," ")
            message= message.replace(" "," ")
        messages.append(message)

train_size=int(len(messages)*training_portion)
train_messages=messages[0: train_size]
train_cat=cat[0:train_size]
v_messages=messages[train_size:]
v_cat=cat[train_size:]


tokenizer =Tokenizer(num_words=vocab_size,oov_token=oov_tok)
tokenizer.fit_on_texts(train_messages)
word_index=tokenizer.word_index


train_sequences=tokenizer.texts_to_sequences(train_messages)
train_paded =pad_sequences(train_sequences,maxlen=max_length)



validation_sequences=tokenizer.texts_to_sequences(v_messages)
validation_paded =pad_sequences(validation_sequences,maxlen=max_length)



cat_tokenizer=Tokenizer()
cat_tokenizer.fit_on_texts(cat)


training_cat_seq =np.hstack(cat_tokenizer.texts_to_sequences(train_cat))



v_cat_seq=np.hstack(cat_tokenizer.texts_to_sequences(v_cat))




model = Sequential()
model.add(Embedding(vocab_size,embedding_dim ))
model.add(Dropout(0.5))
model.add(Bidirectional(LSTM(embedding_dim)))
model.add(Dense(6,activation='softmax' ))
model.summary()
opt= tf.keras.optimizers.Adam(learning_rate=0.001,decay=1e-6)
model.compile(loss='sparse_categorical_crossentropy',optimizer=opt,metrics=['accuracy'])
num_epoch = 20
tp= np.array(list(x for x in train_paded))
tcs=(training_cat_seq)
vp= np.array(list(x for x in validation_paded))

vcs=(v_cat_seq)
print(set(cat))
history=model.fit(tp,tcs,epochs=num_epoch,validation_data=(vp,vcs),verbose=2)





